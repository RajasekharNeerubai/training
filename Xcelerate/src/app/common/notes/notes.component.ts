import { Component, OnInit } from '@angular/core';

import {NotesInfo} from '../../models/notes';

//import {PopupComponent} from '../../common/popup/popup.component';

@Component({
  selector: 'common-notes',
  templateUrl: './notes.component.html',
  //styleUrls: ['./notes.component.css']
})
export class NotesFeatureComponent implements OnInit {
  display: boolean = false;
  notes: NotesInfo[];
  cols: any[];
  dtOptions: DataTables.Settings = {};

  constructor() { 
    this.cols = [
      { field: 'note', header: 'Note',showData:true },
      { field: 'jobImage', header: 'File Type', showData:true },
      { field: 'noteby', header: 'By', showData:true},
      { field: 'dttm', header: 'Date & Time', showData:true},
  ];
  }
  showDialog() {
    this.display = true;
}
  ngOnInit():void {

//  this.dtOptions = {
//                 pagingType: 'full_numbers',
//                 scrollY:     '190px',
//                 info : false,
//                 paging:       false,
//                 searching: false,
//             };

    this.notes =  [
      { 
        note: 'Additional spares are required',
        jobImage:'./assets/images/Water.png',
        noteby:'Andrea',
        dttm:'01/20/2018, 10:00PM'
      },
      { 
        note: 'Switch Off the Motor after cleaning',
        jobImage:'./assets/images/Mold.png',
        noteby:'Conrad',
        dttm:'02/10/2018, 2:00PM'
      },
      { 
        note: 'Get All Necessary Equipments',
        jobImage:'./assets/images/Content.png',
        noteby:'Herbert',
        dttm:'02/14/2018, 5:00PM'
      },
      { 
        note: 'Estimate Revised and Uploaded',
        jobImage:'./assets/images/Asbestos.png',
        noteby:'Wendy',
        dttm:'02/16/2018, 10:00AM'
      },
      { 
        note: 'Waiting for Approval from Adjuster',
        jobImage:'./assets/images/Reconstruction.png',
        noteby:'Timmy',
        dttm:'02/20/2018, 6:00PM'
      },
      { 
        note: 'Expecting revised estimate',
        jobImage:'./assets/images/Bio.png',
        noteby:'Kellie',
        dttm:'03/02/2018, 10:00AM'
      },
      { 
        note: 'First stage of the Contract is completed',
        jobImage:'./assets/images/Water.png',
        noteby:'Dianna',
        dttm:'03/04/2018, 11:00AM'
      },
      { 
        note: 'Waiting for the equipment for the second stage',
        jobImage:'./assets/images/Mold.png',
        noteby:'Roberto',
        dttm:'03/10/2018, 12:00PM'
      },
      { 
        note: 'Waiting for the equipment Approval',
        jobImage:'./assets/images/Content.png',
        noteby:'Reginald',
        dttm:'03/14/2018, 6:00PM'
      },
    ];

  }

}