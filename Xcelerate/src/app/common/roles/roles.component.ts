import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router'
import {HttpClient,HttpHeaders, HttpParams, HttpErrorResponse} from '@angular/common/http';

import {RolesInfo} from '../../models/roles';
import {APIUrls} from '../../shared/constants/apiurls';

//import {PopupComponent} from '../../common/popup/popup.component';

@Component({
  selector: 'common-roles',
  templateUrl: './roles.component.html',
  //styleUrls: ['./notes.component.css']
})
export class RolesComponent implements OnInit {
  display: boolean = false;
  roles: RolesInfo[];
  sub:any;
  id:string;
  url:string;
  myData:any;
  isError:boolean;
  parameterID:string;
  rolesData:any;
  rolePerson:string;

  constructor(private http: HttpClient, private route: ActivatedRoute) { }
  showDialog() {
    this.display = true;
}
getRoles(){
    this.sub = this.route.params.subscribe(params => {
    this.id = params['id']}); 
    this.parameterID=this.id;
   let getParams = new HttpParams().set('jobHashCode', this.parameterID);
  this.url =APIUrls.hosturl+APIUrls.FileRoles;
  console.log('paramter Link '+this.url,{params:getParams});
      this.http.get(this.url,{params:getParams})
          .subscribe(data=>{
            this.myData = data;
            this.rolesData = this.myData.contacts;
            // for(let i=0;i<this.rolesData.length;i++){
            //   this.rolesData[i].fullName= this.rolesData[i].firstName +"  "+this.rolesData[i].lastName;
            // }
           // alert('sucess');

          },(err: HttpErrorResponse) =>{
           // alert('failure');
            if (err.error instanceof Error) {
              console.log("Client-side error occured.");
            } else {
              console.log("Server-side error occured.",err);
            }
          }
        );
}
  ngOnInit():void {
    
   this.getRoles();
    // this.roles =  [
    //   { 
    //     name: 'Supervisor',
    //     mobileNumber:'4564565645',
    //   },
    //   { 
    //     name: 'Estimator',
    //     mobileNumber:'5784577437',
    //   },
    //   { 
    //     name: 'Co-Ordinator',
    //     mobileNumber:'9765634652',
    //   },
    //   { 
    //     name: 'Finance',
    //     mobileNumber:'6456845688',
    //   }
    // ];

  }

}