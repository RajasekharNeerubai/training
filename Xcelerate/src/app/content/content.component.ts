import { Component } from '@angular/core';
import { LocationService} from '../shared/services/location.service';

@Component({
  selector: 'content-body',
  templateUrl: './content.component.html',
  providers: [LocationService]
  //styleUrls: ['./content.component.css']
})
export class ContentComponent  {
isLoggedIn:boolean
constructor() { 
  this.isLoggedIn=false;
}
  
}
