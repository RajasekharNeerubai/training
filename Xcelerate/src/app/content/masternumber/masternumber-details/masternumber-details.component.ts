import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router'
import {HttpClient,HttpHeaders, HttpParams, HttpErrorResponse} from '@angular/common/http';

import {TitleTextService} from '../../../shared/services/titletext.service';
import {APIUrls} from '../../../shared/constants/apiurls';
import {TokenValue} from '../../../shared/services/httpcall/token.constant';
import {Messages} from '../../../shared/constants/messages';
import {MasterNumberData} from '../../../models/masternumber-data';

import {SubJobsGrid} from '../../../models/subjobs-grid';
import {InsuranceDetails} from '../../../models/insurance-details';
import {ContactDetails} from '../../../models/contact-details';
import {FilesPath} from '../../../shared/constants/filespath';
import {GrowlModule,Message} from 'primeng/primeng';


@Component({
  selector: 'app-masternumberdetails',
  templateUrl: './masternumber-details.component.html',
  styleUrls: ['./masternumber-details.component.css']
})
export class MasterNumberDetailsComponent implements OnInit {
    dtOptions: DataTables.Settings = {
      
    };
    sub:any;
    id:string;
    url:string;
    myData:any;
    isError:boolean;
    serverErrorMessage:string;
    masterNumberData: MasterNumberData;
    parameterID:string;
    cols: any[];
    jobImagepath:string = FilesPath.JOBTYPE_ICONS ;
    subJobs: SubJobsGrid[];
    insurances: InsuranceDetails;
    contacts: ContactDetails[];
    refSource: string;
 
    display: boolean = false;
    msgs: Message[] = [];
    mastNumData: any;
    showDialog() {
      this.display = true;
  }
  scroll(el) {
    //alert('scroll');
    el.scrollIntoView();
}
  
 constructor(private data: TitleTextService, private route: ActivatedRoute, private http: HttpClient) {
   this.cols = [
                { field: 'status', header: 'Current Status', showData:true},
                { field: 'subJobCode', header: 'File Number',showData:true },
                { field: 'supervisor', header: 'Supervisor', showData:true},
                { field: 'estimator', header: 'Estimator', showData:true},
                { field: 'jobImage', header: 'File Details',showData:true}
            ];
 }

    getMasterNumberDetails(){
      let getParams = new HttpParams().set('mastNumHash', this.parameterID);
      this.url =APIUrls.hosturl+APIUrls.MasterNumberDetails
      this.http.get(this.url,{params:getParams})
      .subscribe(data=>{
          this.myData = data;
          this.masterNumberData=this.myData.masterNumberDetails;
          this.data.changeMessage("Master Job Number: "+"   "+this.masterNumberData.mastNumCode);
      },
      (err: HttpErrorResponse) => {
          this.isError=true;
          this.serverErrorMessage = Messages.ServerErrorMessage;
            if (err.error instanceof Error) {
              console.log("Client-side error occured.");
            } else {
              console.log("Server-side error occured.");
            }
          }
      );
    }
    getSubJobs(){
        let getParams = new HttpParams().set('mastNumHash', this.parameterID);
        this.url =APIUrls.hosturl+APIUrls.MasterNumberJobs;
        this.http.get(this.url,{params:getParams})
        .subscribe(data=>{
        this.myData=data;
        this.subJobs=this.myData.jobs;
        //console.log('this count'+this.subJobs.length);
        for (let file of this.subJobs) {
                file.style=file.status.toLowerCase().replace(/ /g,"-");
                file.jobImage=this.jobImagepath+file.jobType+".png";
        }
        },
        (err: HttpErrorResponse) => {
                this.isError=true;
                this.serverErrorMessage = Messages.ServerErrorMessage;
                if (err.error instanceof Error) {
                        console.log("Client-side error occured.");
                } else {
                        console.log("Server-side error occured.");
                }
        }
        );
    }
    getInsuranceDetails(){
      const params = new HttpParams().set('mastNumHash',this.parameterID);
      this.url =APIUrls.hosturl+APIUrls.MasterNumberInsuranceDetails;
      this.http.get(this.url,{params:params})
        .subscribe(data=>{
          this.myData=data;
          this.insurances=this.myData.insuranceDetails;
        },
        (err: HttpErrorResponse) => {
            this.isError=true;
            this.serverErrorMessage = Messages.ServerErrorMessage;
            if (err.error instanceof Error) {
              console.log("Client-side error occured.");
            } else {
              console.log("Server-side error occured.");
            }
          }
        );
    }

    getContacts() {
      const params = new HttpParams().set('mastNumHash',this.parameterID);
      this.url =APIUrls.hosturl+APIUrls.MasterNumberContactDetails
      this.http.get(this.url,{params:params})
          .subscribe(data=>{
            this.myData=data;
            this.contacts=this.myData.contacts;
            this.refSource=this.myData.referredSource;
          },
          (err: HttpErrorResponse) => {
              this.isError=true;
              this.serverErrorMessage = Messages.ServerErrorMessage;
              if (err.error instanceof Error) {
                console.log("Client-side error occured.");
              } else {
                console.log("Server-side error occured.");
              }
            }
          );
   }
    ngOnInit() {
      this.dtOptions = {
        pagingType: 'full_numbers',
        order:[],
      }
      this.sub = this.route.params.subscribe(params => {
      this.id = params['id']}); 
      this.parameterID=this.id;
      this.getMasterNumberDetails();
      this.getSubJobs();
      this.getInsuranceDetails();
      this.getContacts();
    }

    updateDescription($event){
      this.mastNumData={};
      this.mastNumData.mastNumId=this.masterNumberData.mastNumId;
      this.mastNumData.mastNumDesc=$event.description;
      this.url=APIUrls.hosturl+APIUrls.UpdateMasterNumberDescription;
      this.http.post(this.url, this.mastNumData)
        .subscribe(data=>{
          this.myData=data;
          this.showSuccess(this.myData.message);
        },
        (err: HttpErrorResponse) => {
            this.isError=true;
            this.serverErrorMessage = Messages.ServerErrorMessage;
            if (err.error instanceof Error) {
              console.log("Client-side error occured.",err);
            } else {
              console.log("Server-side error occured.",err);
            }
          }
        );
     }

     updateContacts($event){
       console.log($event.referredSource);
       console.log($event.contacts);
        this.mastNumData={};
        this.mastNumData.mastNumId=this.masterNumberData.mastNumId;
        this.mastNumData.referredSource=$event.referredSource;
        this.mastNumData.contacts=$event.contacts;
        this.url=APIUrls.hosturl+APIUrls.UpdateMasterNumberContacts;
        this.http.post(this.url, this.mastNumData)
          .subscribe(data=>{
          this.myData=data;
          this.showSuccess(this.myData.message);
          this.getContacts();
        },
        (err: HttpErrorResponse) => {
            this.isError=true;
            this.serverErrorMessage = Messages.ServerErrorMessage;
            if (err.error instanceof Error) {
              console.log("Client-side error occured.",err);
            } else {
              console.log("Server-side error occured.",err);
            }
          }
        );
     }

    updateMasterNumberData($event){
      this.mastNumData=$event.masterNumberData;
        this.mastNumData.mastNumId=this.masterNumberData.mastNumId;
        this.url=APIUrls.hosturl+APIUrls.UpdateMasterNumberDetails;
        console.log(this.mastNumData);
        this.http.post(this.url, this.mastNumData)
          .subscribe(data=>{
          this.myData=data;
          this.showSuccess(this.myData.message);
          this.getMasterNumberDetails();
        },
        (err: HttpErrorResponse) => {
            this.isError=true;
            this.serverErrorMessage = Messages.ServerErrorMessage;
            if (err.error instanceof Error) {
              console.log("Client-side error occured.",err);
            } else {
              console.log("Server-side error occured.",err);
            }
          }
        );
    }

    showSuccess(message) {
        this.msgs = [];
        this.msgs.push({severity:'success', summary:'Success Message', detail:message});
    }


     


}