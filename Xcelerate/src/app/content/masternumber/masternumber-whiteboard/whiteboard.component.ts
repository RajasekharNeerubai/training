import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import {WhiteBoardInfo} from '../../../models/whiteboard';

@Component({
  selector: 'masternumber-whiteboard',
  templateUrl: './whiteboard.component.html',
  //styleUrls: ['./whiteboard.component.css']
})
export class WhiteboardComponent implements OnInit {
  @Input() description: string;
  @Output() updateDesc = new EventEmitter<any>();
  display: boolean = false;
    showDialog() {
      this.display = true;
  }
  constructor() { }

  ngOnInit():void {

  }

  updateDescription(){
    this.updateDesc.emit({
      description: this.description
    });
    this.display=false;
  }

}