import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router'
import {HttpClient,HttpHeaders, HttpParams, HttpErrorResponse} from '@angular/common/http';

import {TitleTextService} from '../../../shared/services/titletext.service';
import {APIUrls} from '../../../shared/constants/apiurls';
import {Messages} from '../../../shared/constants/messages';
//import {SubJobTask} from '../../../models/subjobs-task';

@Component({
  selector: 'subjobs-data',
  templateUrl: './subjobs-data.component.html',
})


export class SubJobsDataComponent implements OnInit {

  sub:any;
  id:string;
  url:string;
  myData:any;
  isError:boolean;
  serverErrorMessage:string;
  fileDetails: any;
  parameterID:string;

  constructor(private data: TitleTextService, private route: ActivatedRoute, private http: HttpClient) {
  }

  display: boolean = false;
    showDialog() {
      this.display = true;
  }
 
  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
    this.id = params['id']}); 
    this.parameterID=this.id;
    let getParams = new HttpParams().set('jobHashCode', this.parameterID);
    console.log('In master number', this.parameterID);
    this.url =APIUrls.hosturl+APIUrls.FileDetails;
    this.http.get(this.url,{params:getParams})
    .subscribe(data=>{
        this.myData = data;
        console.log("In Master Details ",data);
        this.fileDetails=this.myData.jobDetails;
        this.data.changeMessage("File : "+"   "+this.fileDetails.jobCode);
       },
      (err: HttpErrorResponse) => {
          this.isError=true;
          this.serverErrorMessage = Messages.ServerErrorMessage;
          if (err.error instanceof Error) {
            console.log("Client-side error occured.");
          } else {
            console.log("Server-side error occured.");
          }
        }
      );
  }

}
