export interface RouteInfo {
    path: string;
    title: string;
    imgsrc: string;
    class: string;
    activeImage:string;
    redirectTo:string;
    pathMatch:string;
}