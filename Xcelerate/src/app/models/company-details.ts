export class CompanyDetails{
    companyId : number;
	companyName:string;
	companyCode:string;
    companyHashCode:string;
	address:string;
	city:string;
	state:string;
	zip:string;
	phone1:string;
	phone2:string;
	email:string;
	name:string;
}