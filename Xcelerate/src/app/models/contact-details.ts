export class ContactDetails {
    id:number;
    firstName: string;
    lastName: string;
    phoneNo:string;
    mobile:string;
    address:string;
    contactType:string;
    status:string;
    fullName:string;
  }