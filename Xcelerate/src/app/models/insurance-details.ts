export class InsuranceDetails{
     insuranceId: number;
     claimNumber: string;
     description: string;
     insuranceCompanyName: string;
     policyNumber: string;
     status: string;
     tpaName: string;
     insuranceCompany: string;
     address: string;
     tpa: string;
     sourceId: number;
}