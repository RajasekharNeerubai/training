export interface OwnerDetails{
      OwnerId:number,
      OwnerName:string,
      fullName:string,
      email: string,
      address:string,
      lossAddressSame:boolean,
      lossAddress:string,
      billingAddressSame:boolean,
      billingAddress:string,
      homePhone
      cellPhone
      workNumber
      tenantName
      tenantNumber
}

