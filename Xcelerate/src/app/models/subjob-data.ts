export class SubJobData{
    jobId: number;
    actualAmount: number;
    actualJobId: number;
    budgetedAmount: number;
    city: string;
    currentJobStatus: string;
    estimatedAmount: number;
    geoLocation: string;
    invoicedAmount: number;
    isDuplicate: string;
    isInsured: string;
    jobAmount: number;
    jobCode: string;
    jobDesc: string;
    jobHashCode: string;
    paymentTermsId: number;
    jobType: string;
    lossAddress: string;
    billingAddress: string;
    state: string;
    zip: string;
    latitude: string;
    longitude: string;
    masterNumberCopy: string;
    createdBy: number;
    createdDatetime: string;
    updatedBy: number;
    updatedDatetime: string;

}