export interface RouteInfo {
    path: string;
    title: string;
    imgsrc: string;
    class: string;
}