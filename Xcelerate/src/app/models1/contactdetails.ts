export class ContactDetails {
    id:string;
    firstName: number;
    lastName: string;
    phoneNo:string;
    mobile:string;
    address:string;
    contactType:string;
    status:string;
  }