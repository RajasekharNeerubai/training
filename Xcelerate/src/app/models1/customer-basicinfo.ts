export class CustomerBasicInfo {
    customerId:number;
    firstName:string;
    lastName:string;
    email: string;
    address1: string;
    adress2: string;
    city:string;
    state:String;
    zip: String;
    phone1:string;
    mobile:string;
    workPhone:string;
  }

     