
export const APIUrls = {
 //hosturl: 'http://192.168.1.100:8081/', 
  hosturl: 'http://dev.wisseninfotech.com:8081/', 
  DashboardMasterNumbersCount: 'xcelerate/dashboard/masterNumbers',
  DashboardJobsCount: 'xcelerate/dashboard/jobs',
  DashboardMyJobsCount: 'xcelerate/dashboard/myJobs',
  DashboardCustomersCount: 'xcelerate/dashboard/customers',
  DashboardTpaJobsCount: 'xcelerate/dashboard/jobsByTpa',
  DashboardMapCount: 'xcelerate/dashboard/jobLocations',
  AuthenticateUser:'xcelerate/login',
  MasterNumberCompanies:'xcelerate/externalagency/companies',
  MasterNumberInsuranceCompanies:'xcelerate/externalagency/insuranceCompanies',
  MasterNumberInsuranceAgents:'xcelerate/contacts/insuranceAgents',
  MasterNumberCustomers:'xcelerate/customers',
  MasterNumberConfigurationData:'xcelerate/configdata/newMasterNumber',
  MasterNumberGrid:'xcelerate/masternumber/masterNumbers',
  MasterNumberMyGrid:'xcelerate/masternumber/myMasterNumbers',
  SaveMasterData:'xcelerate/masternumber/saveMasterNumber',
  UpdateMasterNumberDescription:'xcelerate/masternumber/updateDescription',
  UpdateMasterNumberContacts:'xcelerate/masternumber/updateContacts',
  UpdateMasterNumberDetails:'xcelerate/masternumber/updateMasterNumber',
  MasterNumberDetails:'xcelerate/masternumber/details',
  MasterNumberJobs:'xcelerate/masternumber/jobs',
  MasterNumberContactDetails:'xcelerate/masternumber/contacts',
  MasterNumberInsuranceDetails:'xcelerate/masternumber/insuranceDetails',
  MasterNumberAdjusters:'xcelerate/contacts/adjusters',
  MasterNumberSalesRepresentative:'xcelerate/contacts/salesRepresentatives',
  MasterNumberProjectManager:'xcelerate/contacts/projectManagers',
  MasterNumberPropertyManager:'xcelerate/contacts/propertyManagers',
  MasterNumberReferredBy:'xcelerate/contacts/all',
  MasterNumberTpa:'xcelerate/contacts/tpa',
  MasterNumberProgramType:'xcelerate/programType/programTypes',
  SaveContactDetails:'xcelerate/contacts/saveContactDetails',
  SaveCompanyDetails:'xcelerate/externalagency/saveExternalAgency',
  NewFileEstimator:'xcelerate/contacts/estimators',
  NewFileSupervisor:'xcelerate/contacts/supervisors',
  NewFileFinance:'xcelerate/contacts/finance',
  NewFileCoordinator:'xcelerate/contacts/coordinators',
  SaveFileDetails:'xcelerate/job/saveJobDetails',
  FileLink:'fileLink/',
  MyFilesGrid: 'xcelerate/job/myJobs',
  FilesGrid: 'xcelerate/job/jobs',
  AttentionRequiredFilesGrid: 'xcelerate/job/attentionRequired',
  FileDetails: 'xcelerate/job/details',
  FileRoles: 'xcelerate/job/roles'
};

