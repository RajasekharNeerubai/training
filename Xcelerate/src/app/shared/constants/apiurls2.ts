
export const APIUrls = {
 //hosturl: 'http://192.168.55.27:8081/', 
  hosturl: 'http://dev.wisseninfotech.com:8081/', 
  DashboardMasterNumbersCount: 'xcelerate/dashboard/masterNumbers',
  DashboardJobsCount: 'xcelerate/dashboard/jobs',
  DashboardCustomersCount: 'xcelerate/dashboard/customers',
  DashboardTpaJobsCount: 'xcelerate/dashboard/jobsByTpa',
  DashboardMapCount: 'xcelerate/dashboard/jobLocations',
  AuthenticateUser:'xcelerate/login',
  MasterNumberInsuranceCompanies:'xcelerate/externalagency/insuranceCompanies',
  MasterNumberInsuranceAgents:'xcelerate/contacts/insuranceAgents',
  MasterNumberCustomers:'xcelerate/customers',
  MasterNumberConfigurationData:'xcelerate/configdata/newMasterNumber',
  MasterNumberGrid:'xcelerate/masternumber/masterNumbers?locationHash=Loc12345',
  MasterNumberMyGrid:'xcelerate/masternumber/myMasterNumbers?locationHash=Loc12345',
  SaveMasterData:'xcelerate/masternumber/saveMasterNumber',
  MasterNumberDetails:'xcelerate/masternumber/details',
  MasterNumberContactDetails:'xcelerate/masternumber/contacts',
  MasterNumberAdjusters:'xcelerate/contacts/adjusters',
  MasterNumberSalesRepresentative:'xcelerate/contacts/salesRepresentatives',
  MasterNumberProjectManager:'xcelerate/contacts/projectManagers',
  MasterNumberPropertyManager:'xcelerate/contacts/propertyManagers',
  MasterNumberReferredBy:'xcelerate/contacts/all',
  MasterNumberTpa:'xcelerate/contacts/tpa',
  MasterNumberProgramType:'xcelerate/programType/programTypes',
  SaveContactDetails:'xcelerate/contacts/saveContactDetails'
};

